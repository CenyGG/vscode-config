````
{
    "editor.smoothScrolling": true,
    "workbench.panel.defaultLocation": "bottom",
    "editor.minimap.enabled": false,
    "files.autoSave": "afterDelay",
    "files.autoSaveDelay": 300,
    "terminal.integrated.shell.osx": "/bin/bash",
    "workbench.colorTheme": "One Dark Pro Bold",
    "workbench.iconTheme": "material-icon-theme",
    "editor.defaultFormatter": "esbenp.prettier-vscode"
}
````